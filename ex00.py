class Person:
    def __init__(self, name, age, address):
        self.name = name
        self.age = age
        self.address = address

    def set_name(self, name):
        self.name = name

    def set_age(self, age):
        self.age = age

    def set_address(self, address):
        self.address = address

    def get_name(self):
        return self.name

    def get_age(self):
        return self.age

    def get_address(self):
        return self.address

    def display_info(self):
        print(f"Name{':':>8} {self.name}\nAge{':':>9} {self.age}\nAddress{':':>5} {self.address}")

class Student(Person):
    def __init__(self, name, age, address, student_id, major, gpa):
        super().__init__(name, age, address)
        self.student_id = student_id
        self.major = major
        self.gpa = gpa

    def set_student_id(self, student_id):
        self.student_id = student_id

    def set_major(self, major):
        self.major = major

    def set_gpa(self, gpa):
        self.gpa = gpa

    def get_student_id(self):
        return self.student_id

    def get_major(self):
        return self.major

    def get_gpa(self):
        return self.gpa

    def display_info(self):
        super().display_info()
        print(f"Student ID{':':>2} {self.student_id}\nMajor{':':>7} {self.major}\nGPA{':':>9} {self.gpa}")

# BankAccount class
class BankAccount:
    def __init__(self, account_number, account_holder_name, balance=0):
        self.account_number = account_number
        self.account_holder_name = account_holder_name
        self._balance = balance  # Encapsulating the balance attribute (Private)

    def deposit(self, amount):
        if amount > 0:
            self._balance += amount
            print(f"Deposited ${amount}. Current Balance: ${self._balance}")
        else:
            print("Deposit amount must be greater than 0.")

    def withdraw(self, amount):
        if 0 < amount <= self._balance:
            self._balance -= amount
            print(f"Withdrew ${amount}. Current Balance: ${self._balance}")
        else:
            print("Insufficient balance or invalid withdrawal amount.")

    def display_balance(self):
        print(f"Account Holder{':':>2} {self.account_holder_name}\nAccount Number{':':>2} {self.account_number}\nBalance{':':>9} ${self._balance}")

if __name__ == "__main__":
    student = Student("John Doe", 25, "123 Main Street", "1234567", "Computer Science", 3.8)
    student.display_info()
    print()
    account = BankAccount("1234567890", "John Doe", 100)
    account.display_balance()
    account.deposit(500)
    account.withdraw(200)
    account.display_balance()

