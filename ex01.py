class Book:
    def __init__(self, title, author, genre):
        self.title = title
        self.author = author
        self.genre = genre
        self.is_available = True

    def display_info(self):
		print("[BOOK INFORMATION]")
        print(f"Book Title: {self.title}")
		print(f"Author: {self.author}")
		print(f"Genre: {self.genre}")
		availability = "Yes" if self.is_available else "No"
		print(f"Available: {availability}")

    def setAvailable(self):

class Member:
    def __init__(self, name, address):
        self.name = name
        self.address = address
        self.borrowed_books = []

    def display_info(self):

    def borrow_book(self, book):

    def return_book(self, book):

class Library:
    def __init__(self):
        self.books = []
        self.members = []

    def add_book(self, title, author, genre):

    def add_member(self, name, address):

    def display_books(self):

    def display_members(self):

    def borrow_book(self, book_title, member_name):

    def return_book(self, book_title, member_name):

def main():

if __name__ == "__main__":
    main()

